package de.vps.fame.base

import android.app.Application
import de.vps.fame.model.Place
import de.vps.fame.model.Rubrik

class AppApplication : Application() {
    private var places: ArrayList<Place> = ArrayList()
    private var rubriks: ArrayList<Rubrik> = ArrayList()

    private var emergencyNumbers : ArrayList<Place> = ArrayList()

    init {
        instance = this
    }

    companion object {
        private var instance: AppApplication? = null

        @JvmStatic
        var poiList: ArrayList<Place>
            get() = instance!!.places
            set(poiList) {
                instance!!.places = poiList
            }

        @JvmStatic
        var rubrikList: ArrayList<Rubrik>
            get() = instance!!.rubriks
            set(rubrikList) {
                instance!!.rubriks = rubrikList
            }

        @JvmStatic
        var emergencyNumberList: ArrayList<Place>
            get() = instance!!.emergencyNumbers
            set(emergencyNumberList) {
                instance!!.emergencyNumbers = emergencyNumberList
            }
    }


}