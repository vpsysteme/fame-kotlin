package de.vps.fame.view.activity

import android.Manifest
import android.annotation.SuppressLint
import android.content.Context
import android.content.Intent
import android.content.pm.PackageManager
import android.graphics.Typeface
import android.net.Uri
import android.os.Bundle
import android.os.Looper
import android.view.LayoutInflater
import android.view.View
import android.widget.*
import android.widget.AdapterView.OnItemSelectedListener
import androidx.appcompat.app.AppCompatActivity
import androidx.appcompat.widget.Toolbar
import androidx.core.app.ActivityCompat
import androidx.core.view.isVisible
import androidx.fragment.app.FragmentTransaction
import com.google.android.gms.common.ConnectionResult
import com.google.android.gms.common.api.GoogleApiClient
import com.google.android.gms.common.api.GoogleApiClient.ConnectionCallbacks
import com.google.android.gms.common.api.GoogleApiClient.OnConnectionFailedListener
import com.google.android.gms.location.*
import com.google.android.gms.maps.CameraUpdateFactory
import com.google.android.gms.maps.GoogleMap
import com.google.android.gms.maps.OnMapReadyCallback
import com.google.android.gms.maps.SupportMapFragment
import com.google.android.gms.maps.model.CameraPosition
import com.google.android.gms.maps.model.LatLng
import com.google.android.gms.maps.model.Marker
import com.google.android.gms.maps.model.MarkerOptions
import com.google.android.material.bottomsheet.BottomSheetBehavior
import com.google.android.material.bottomsheet.BottomSheetBehavior.BottomSheetCallback
import de.vps.fame.R
import de.vps.fame.base.AppApplication
import de.vps.fame.model.Place
import de.vps.fame.tool.FameConst
import de.vps.fame.tool.FameConst.PLACE_STATE_ALL
import de.vps.fame.tool.FameConst.PLACE_STATE_KEY
import de.vps.fame.tool.FameConst.PLACE_STATE_POI
import de.vps.fame.tool.FameConst.PLACE_STATE_RUBRIK
import de.vps.fame.tool.FameConst.PLACE_STATE_THEMA
import de.vps.fame.tool.FameConst.POI_KEY
import de.vps.fame.tool.FameConst.RUBRIK_KEY
import de.vps.fame.tool.FameConst.THEMA_KEY
import de.vps.fame.tool.Logger
import de.vps.fame.tool.PhoneTool
import de.vps.fame.view.dialog.CallDialogFragment
import de.vps.fame.view.fragment.CallFragment
import kotlinx.android.synthetic.*

//TODO für das Auswahlmenu wurde in der alten App mit einen spinner umgesetz.
// Die Sichtbarkeit des Spinner wurde mit einen Button (icon) in der toolbar geregelt.
// Für den spinner gab es bisjetz eine default Auswahl. Das sollten nicht umbedingt alle POI
// sein, weil die Erzeugung und Anzeige aller POI auf der Map auf ältern Geräten sehr langsam ist.

class MapActivity : AppCompatActivity(), OnMapReadyCallback, ConnectionCallbacks,
    OnConnectionFailedListener {

    private var mCurrentLoc = LatLng(FameConst.LAT_BOCHUM, FameConst.LON_BOCHUM)
    private lateinit var fusedLocClient: FusedLocationProviderClient
    private var mGoogleApiClient: GoogleApiClient? = null
    private lateinit var mMap: GoogleMap
    private var locMarker: Marker? = null
    private lateinit var bottomSheetBehavior: BottomSheetBehavior<View>

    private lateinit var llBottomSheet: LinearLayout


    //private Marker mStandPoint;
    private var mPoiSpinner: Spinner? = null
    private var mSpinnerLayout: LinearLayout? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_maps)
        // Obtain the SupportMapFragment and get notified when the map is ready to be used.
        val mapFragment = supportFragmentManager
            .findFragmentById(R.id.map) as SupportMapFragment
        mapFragment.getMapAsync(this)

        llBottomSheet = findViewById(R.id.bottom_sheet)
        bottomSheetBehavior = BottomSheetBehavior.from(llBottomSheet)
        //bottomSheetBehavior.setState(BottomSheetBehavior.STATE_COLLAPSED);
        //bottomSheetBehavior.setState(BottomSheetBehavior.STATE_EXPANDED);
        bottomSheetBehavior.state = BottomSheetBehavior.STATE_HIDDEN

        initLocationRequest()

        mPoiSpinner = findViewById(R.id.map_spinner_rubrik)
        mSpinnerLayout = findViewById(R.id.map_spinner_layout)


        //isSpinnerOpen = false;
        mSpinnerLayout?.setVisibility(View.GONE)
        mPoiSpinner?.setVisibility(View.VISIBLE)

        // erzeuge Toolbar für die Action bar
        val bar = findViewById<Toolbar>(R.id.toolbar_map)
        findViewById<TextView>(R.id.toolbar_title).text = getString(R.string.frag_main_map_title)

        // TODO Das Icon für den Adapter in der Toolbar ist R.id.toolbar_image im xml layout.activity_maps


        setSupportActionBar(bar)

        val image = bar.findViewById<ImageView>(R.id.toolbar_image)
        image.visibility = View.VISIBLE
        image.setImageResource(R.drawable.ic_filter_map)
        bar.findViewById<View>(R.id.toolbar_image_touch)
            .setOnClickListener {

                if (mSpinnerLayout!!.isVisible) {
                    Logger.e("aaaaaaaaaa")
                    mSpinnerLayout?.setVisibility(View.GONE)
                } else {
                    Logger.e("bbbbbbbbbb")
                    mSpinnerLayout?.setVisibility(View.VISIBLE)
                    //mPoiSpinner?.performClick()
                }
            }

    }

    override fun onResume() {
        super.onResume()

        // die Größe kann nur hier geändert werden, weil der View erst nach onCreate erzeugt wird.
        resizeBottomSheet()
    }

    // Diese Methode wird aufgerufen, wenn eine Map bereit ist
    override fun onMapReady(googleMap: GoogleMap) {
        mMap = googleMap

        mMap.setOnMapClickListener {
            bottomSheetBehavior.isHideable = true
            bottomSheetBehavior.setState(BottomSheetBehavior.STATE_HIDDEN)
        }

        initUiSettings()

        val poiList = createPoiList()
        //Hier wird zum erstenmal eine Liste geladem


        for (poi in poiList) {
            val poiPoint = LatLng(poi.lat, poi.lon)
            val maker = mMap.addMarker(
                MarkerOptions().position(poiPoint).title(poi.name)
                    .snippet(poi.rubrik)
            )
            maker.tag = poi.id
        }

        val poiLoc = if (poiList.size == 1) {
            LatLng(poiList[0].lat, poiList[0].lon)
        } else if (intent.getIntExtra(PLACE_STATE_KEY, -1) ==  PLACE_STATE_THEMA) {
            LatLng(poiList[0].lat, poiList[0].lon)
        } else {
            LatLng(FameConst.LAT_BOCHUM, FameConst.LON_BOCHUM)
        }

        goToCameraPosition(poiLoc)


        mMap.setOnMarkerClickListener { marker ->
            bottomSheetBehavior.isHideable = true
            bottomSheetBehavior.state = BottomSheetBehavior.STATE_HIDDEN
            marker?.showInfoWindow()

            false
        }

        mMap.setOnInfoWindowClickListener { marker ->
                if (marker != locMarker) {
                    val pois = AppApplication.poiList
                    val poi = pois.first { "".plus(it.id) == marker!!.tag }
                    createPoiSheet(poi)
                }
        }
        //TODO <<Ende Auslagerung>>

        if (intent.getIntExtra(PLACE_STATE_KEY, -1) == PLACE_STATE_POI) {
            runOnUiThread {
                createPoiSheet(poiList[0])

            }
        }

        buildSpinnerMenu()
    }

    private fun buildSpinnerMenu() {

        val rubrikNameList = ArrayList<String>()
        rubrikNameList.add("Alle Orte")
        AppApplication.rubrikList.forEach { rubrikNameList.add(it.name) }

        val poiAdapter: ArrayAdapter<String> =
            ArrayAdapter(this, R.layout.simple_spinner_item, rubrikNameList)
        poiAdapter.setDropDownViewResource(R.layout.simple_spinner_dropdown_item)
        mPoiSpinner?.setAdapter(poiAdapter)


        when (intent.getIntExtra(PLACE_STATE_KEY, -1) ) {
            PLACE_STATE_ALL ->mPoiSpinner?.setSelection(0)
            PLACE_STATE_RUBRIK -> {
                val rubrik = intent.getStringExtra(RUBRIK_KEY)
                val size: Int = rubrikNameList.size
                var posi = 1
                for (i in 0 until size) {
                    if (rubrikNameList[i] === rubrik) {
                        posi = i + 1
                        break
                    }
                }
                mPoiSpinner?.setSelection(posi)
            }
        }

        //
        mPoiSpinner?.setOnItemSelectedListener(object : OnItemSelectedListener {
            override fun onItemSelected(
                parent: AdapterView<*>?,
                view: View,
                position: Int,
                id: Long
            ) {
                mMap.clear()

                bottomSheetBehavior.isHideable = true
                bottomSheetBehavior.setState(BottomSheetBehavior.STATE_HIDDEN)

                val listPoi = if (position==0) {
                    AppApplication.poiList
                } else {
                    AppApplication.poiList.filter { it.rubrik == rubrikNameList[position] }
                }

                listPoi.forEach{
                    val poiPoint = LatLng(it.lat, it.lon)
                    val maker = mMap.addMarker(
                        MarkerOptions().position(poiPoint).title(it.name)
                            .snippet(it.rubrik)
                    )
                    maker.tag = it.id
               }

                if (listPoi.size > 0)
                    goToCameraPosition(LatLng(listPoi[0].lat, listPoi[0].lon))


                mMap.setOnMarkerClickListener { marker ->
                    bottomSheetBehavior.isHideable = true
                    bottomSheetBehavior.state = BottomSheetBehavior.STATE_HIDDEN
                    marker?.showInfoWindow()

                    false
                }

                mMap.setOnInfoWindowClickListener { marker ->
                    if (marker != locMarker) {
                        val pois = AppApplication.poiList
                        val poi = pois.first { "".plus(it.id) == marker!!.tag }
                        createPoiSheet(poi)
                    }
                }

            }

            override fun onNothingSelected(parent: AdapterView<*>?) {
                // Do nothing
            }
        })
    }

    private fun createPoiSheet(poi : Place) {

        bottomSheetBehavior.isHideable = false
        bottomSheetBehavior.state = BottomSheetBehavior.STATE_EXPANDED
        bottomSheetBehavior.addBottomSheetCallback(object : BottomSheetCallback() {
            override fun onStateChanged(
                bottomSheet: View,
                newState: Int
            ) {
                if (newState == BottomSheetBehavior.STATE_DRAGGING) {
                    bottomSheetBehavior.state = BottomSheetBehavior.STATE_EXPANDED
                }

            }

            override fun onSlide(
                bottomSheet: View,
                slideOffset: Float
            ) {
            }
        })

        findViewById<View>(R.id.bottom_sheet_route).setOnClickListener {
            try {

                val intent = Intent(
                    Intent.ACTION_VIEW,
                    Uri.parse(
                        "http://maps.google.com/maps?saddr="
                                + mCurrentLoc.latitude + ","
                                + mCurrentLoc.longitude
                                + "&daddr=" + poi.lat + ","
                                + poi.lon
                    )
                )
                intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK)
                intent.addCategory(Intent.CATEGORY_LAUNCHER)
                intent.setClassName(
                    "com.google.android.apps.maps",
                    "com.google.android.maps.MapsActivity"
                )
                startActivity(intent)
            } catch (e: Exception) {
                Logger.e("Exception start route map", e)
            }
        }

        findViewById<TextView>(R.id.bottom_sheet_text_1).text = poi.name
        val linearLayout = findViewById<LinearLayout>(R.id.bottom_sheet_scroll)
        linearLayout.removeAllViews()

        if (poi.name.isNotEmpty()) {
//                    linearLayout.addView(
//                        getItemView(
//                            getString(R.string.map_info_rubrik),
//                            poi.rubrik
//                        )
//                    )
            linearLayout.addView(
                getItemView("", poi.rubrik)
            )
        }
        /*
                if (poi.thema.isNotEmpty()) {
                    linearLayout.addView(
                        getItemView(
                            getString(R.string.map_info_thema),
                            poi.thema
                        )
                    )
                }
        */
        if (poi.ansprechpartner.isNotEmpty()) {
            linearLayout.addView(
                getItemView(
                    getString(R.string.map_info_ansprechpartner),
                    poi.ansprechpartner
                )
            )
        }
        if (poi.strasse.isNotEmpty()) {
            linearLayout.addView(
                getItemView(
                    getString(R.string.map_info_strasse),
                    poi.strasse
                )
            )
        }
        if (poi.plz.isNotEmpty() && poi.ort.isNotEmpty()) {
            linearLayout.addView(
                getItemPlaceView(
                    getString(R.string.map_info_plz),
                    getString(R.string.map_info_ort),
                    poi.plz,
                    poi.ort
                )
            )
        } else {
            if (poi.plz.isNotEmpty()) {
                linearLayout.addView(
                    getItemView(
                        getString(R.string.map_info_plz),
                        poi.plz
                    )
                )
            }
            if (poi.ort.isNotEmpty()) {
                linearLayout.addView(
                    getItemView(
                        getString(R.string.map_info_ort),
                        poi.ort
                    )
                )
            }
        }
        val phoneNumber = poi.telefon
        if (PhoneTool.validPhoneNumber(phoneNumber)) {
            val phoneView = getItemView(
                getString(R.string.map_info_tel),
                phoneNumber,
                R.color.colorLink
            )
            phoneView.setOnClickListener { callPhoneNumber(phoneNumber) }
            linearLayout.addView(phoneView)
        }
        val emailUrl = poi.email
        if (emailUrl.isNotEmpty()) {
            val emailView = getItemView(
                getString(R.string.map_info_email),
                emailUrl,
                R.color.colorLink
            )
            linearLayout.addView(emailView)
            emailView.setOnClickListener {
                val emailIntent = Intent(Intent.ACTION_SEND)
                val emailList = arrayOf(emailUrl)
                emailIntent.putExtra(Intent.EXTRA_EMAIL, emailList)
                emailIntent.type = "plain/text"
                startActivity(emailIntent)
            }
            /*
            String text = "<a href=\"mailto:"+data.email+"\">"+data.email+"</a>";
            linearLayout.addView(getItemView(getString(R.string.map_info_email),text,true));
            */
        }
        var url = poi.internet
        if (url.isNotEmpty()) {

            val internetView = getItemView(
                getString(R.string.map_info_internet),
                url,
                R.color.colorLink
            )
            internetView.setOnClickListener {

                if (!url.startsWith("http://") && !url.startsWith("https://"))
                    url = "http://$url"

                val browserIntent =
                    Intent(Intent.ACTION_VIEW, Uri.parse(url))
                startActivity(browserIntent)


            }
            linearLayout.addView(internetView)
            /*
            String text = "<a href=\""+data.internet+"\">"+data.internet+"</a>";
            linearLayout.addView(getItemView(getString(R.string.map_info_internet),text,true));
            */
        }
        //if (poi.sozialraum.isNotEmpty()) {
        //    linearLayout.addView(getItemView(getString(R.string.map_info_sozialraum), poi.sozialraum))
        //}
    }

    private fun initLocationRequest() {

        initGoogleApi()

        fusedLocClient = LocationServices.getFusedLocationProviderClient(this)

        if (checkPermission()) {
            requestPermissions(
                arrayOf(
                    Manifest.permission.ACCESS_COARSE_LOCATION,
                    Manifest.permission.ACCESS_FINE_LOCATION
                ),
                FameConst.PERMISSIONS_REQUEST_ACCESS_LOCATION
            )
        }
    }

    private fun initGoogleApi() {
        mGoogleApiClient = GoogleApiClient.Builder(this)
            .addConnectionCallbacks(this)
            .addOnConnectionFailedListener(this)
            .addApi(LocationServices.API)
            .build()
        mGoogleApiClient?.connect()
    }

    private fun initUiSettings() {

        mMap.uiSettings.isCompassEnabled = true
        mMap.uiSettings.isMyLocationButtonEnabled = true
        mMap.uiSettings.isMapToolbarEnabled = true
        mMap.uiSettings.isZoomControlsEnabled = true

        mMap.mapType = GoogleMap.MAP_TYPE_NORMAL
        //mMap.mapType = GoogleMap.MAP_TYPE_SATELLITE
    }

    private fun goToCameraPosition(loc: LatLng) {

        mMap.setMinZoomPreference(8.0f)
        mMap.setMaxZoomPreference(20.0f)

        val camPos = CameraPosition.Builder()
            .target(loc)
            .zoom(14f)
            .build()

        mMap.moveCamera(CameraUpdateFactory.newCameraPosition(camPos))
    }

//    private fun moveCamera() {
//        val dp = 50f
//        Logger.e("dp $dp - px: ${Tool.pxfromDp(this,dp)}")
//        mMap.moveCamera(CameraUpdateFactory.scrollBy(0f, Tool.pxfromDp(this,dp)))
//    }

    override fun onRequestPermissionsResult(
        requestCode: Int,
        permissions: Array<String>,
        grantResults: IntArray
    ) {
        when (requestCode) {
            FameConst.PERMISSIONS_REQUEST_ACCESS_LOCATION -> if (grantResults.isNotEmpty() && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                Logger.i("PERMISSION_GRANTED")
                startLocationUpdates()
            }
        }
    }

    override fun onConnected(bundle: Bundle?) {
        startLocationUpdates()
    }

    override fun onConnectionSuspended(i: Int) {
        mGoogleApiClient!!.connect()
    }

    override fun onConnectionFailed(connectionResult: ConnectionResult) {

        Logger.e("Connection error: ${connectionResult.errorMessage}")
    }

    private fun checkPermission(): Boolean {
        return ActivityCompat.checkSelfPermission(
            this,
            Manifest.permission.ACCESS_FINE_LOCATION
        ) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(
            this, Manifest.permission.ACCESS_COARSE_LOCATION
        ) != PackageManager.PERMISSION_GRANTED
    }

    override fun onStart() {
        super.onStart()
        if (mGoogleApiClient != null) {
            mGoogleApiClient!!.connect()
            Logger.e("client connect")
        }
    }

    override fun onStop() {
        mGoogleApiClient!!.disconnect()
        super.onStop()
    }

    override fun onPause() {
        super.onPause()
        stopLocationUpdates()
    }

    /**
     * start location updates (GPS)
     */
    private fun startLocationUpdates() {
        if (checkPermission()) return
        mMap.isMyLocationEnabled = true

        val locationRequest = LocationRequest()
        locationRequest.interval = FameConst.LOCATION_UPDATE_INTERVAL
        locationRequest.fastestInterval = FameConst.LOCATION_FASTEST_UPDATE_INTERVAL
        locationRequest.priority = LocationRequest.PRIORITY_HIGH_ACCURACY

        fusedLocClient.requestLocationUpdates(locationRequest, mLocationCallback, Looper.myLooper())
    }

    /**
     * stop location update (GPS)
     */
    @SuppressLint("MissingPermission")
    private fun stopLocationUpdates() {
        fusedLocClient.removeLocationUpdates(mLocationCallback)
        try {

            mMap.isMyLocationEnabled = false
        } catch (e: Exception) {
            Logger.e("Error", e)
        }
    }

    private val mLocationCallback: LocationCallback = object : LocationCallback() {
        override fun onLocationResult(locationResult: LocationResult) {

            val loc = locationResult.lastLocation

            mCurrentLoc = LatLng(loc.latitude, loc.longitude)
            /*
            val locationList =
                locationResult.locations
            if (locationList.size > 0) {
                if (locMarker != null) locMarker?.remove()
                val location = locationList.last()
                locMarker= mMap.addMarker(MarkerOptions()
                    .position(LatLng(location.latitude,location.longitude))
                    .icon(BitmapDescriptorFactory.defaultMarker(BitmapDescriptorFactory.HUE_BLUE)))
            }
             */
        }
    }

    private fun getItemView(key: String, text: String, colorResId: Int): View {
        val inflater = getSystemService(Context.LAYOUT_INFLATER_SERVICE) as LayoutInflater

        @SuppressLint("InflateParams")
        val view = inflater.inflate(R.layout.item_poi_list, null)
        val headerView = view.findViewById<TextView>(R.id.item_poi_header)
        val textView = view.findViewById<TextView>(R.id.item_poi_text)

        if (key == "") {
            headerView.visibility = View.GONE
            textView.setTypeface(null,Typeface.BOLD)
        }

        headerView.text = key
        textView.text = text

        if (colorResId != -42)
            textView.setTextColor(resources.getColor(colorResId, null))

        return view
    }

    private fun getItemView(key: String, text: String): View {
        return getItemView(key, text, -42)
    }


    private fun getItemPlaceView(key_plz: String, key_ort: String, plz: String, ort: String): View {
        val inflater = getSystemService(Context.LAYOUT_INFLATER_SERVICE) as LayoutInflater
        val view = inflater.inflate(R.layout.item_poi_list_ort, null)
        (view.findViewById<View>(R.id.item_poi_plz) as TextView).text = key_ort
        (view.findViewById<View>(R.id.item_poi_ort) as TextView).text = key_plz
        view.findViewById<TextView>(R.id.item_poi_plz_text).text = plz
        view.findViewById<TextView>(R.id.item_poi_ort_text).text = ort

        return view
    }

    private fun callPhoneNumber(phoneNumber: String) {

        val number = PhoneTool.getPhoneNumber(phoneNumber)
        if (checkPhoneSupport()) {
            if (FameConst.CAN_PHONE_CALL) {
                val mag: String = getString(R.string.msg_phone_call_one) +
                        "\n\n" + number.replace(" ", "").replace("/", "")
                    .trim { it <= ' ' } + "\n\n" + getString(R.string.msg_phone_call_two)
                val ft: FragmentTransaction =
                    this.supportFragmentManager.beginTransaction()
                CallDialogFragment.newInstance(mag, number)
                    .show(ft, CallFragment::class.java.simpleName)
            } else {
                Toast.makeText(this, "Tel: $number", Toast.LENGTH_SHORT)
                    .show() //For debug
            }
        } else {
            Toast.makeText(this, "Dieses Device unterstütz keine Telefonie", Toast.LENGTH_SHORT)
                .show()
        }
    }

    /**
     * Is true, if the device have a phone module.
     *
     * @return state
     */
    private fun checkPhoneSupport(): Boolean {
        return packageManager!!.hasSystemFeature(PackageManager.FEATURE_TELEPHONY)
    }

    private fun resizeBottomSheet() {

        val h = (resources.displayMetrics.heightPixels*44)/100
        Logger.e("h: $h")

        llBottomSheet.layoutParams.height = h
        llBottomSheet.requestLayout()
    }

    private fun createPoiList(): ArrayList<Place> {
        val list = AppApplication.poiList

        val test = intent.getIntExtra(PLACE_STATE_KEY, -1)

        Logger.e("State: $test")

        return try {
            when (intent.getIntExtra(PLACE_STATE_KEY, -1)) {
                PLACE_STATE_ALL -> list
                PLACE_STATE_RUBRIK -> list.filter { it.rubrik == intent.getStringExtra(RUBRIK_KEY) } as ArrayList<Place>
                PLACE_STATE_POI -> {list.filter {it.id == intent.getStringExtra(POI_KEY)} as ArrayList<Place>}
                PLACE_STATE_THEMA -> list.filter {
                    it.rubrik == intent.getStringExtra(RUBRIK_KEY) && it.thema == intent.getStringExtra(
                        THEMA_KEY
                    )
                } as ArrayList<Place>
                else -> list
            }
        } catch (e: java.lang.Exception) {
            Logger.e("Exception cast poi", e)
            list
        }
    }


}