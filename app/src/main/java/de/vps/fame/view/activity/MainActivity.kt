package de.vps.fame.view.activity

import android.content.ComponentName
import android.content.Context
import android.content.Intent
import android.content.ServiceConnection
import android.os.Bundle
import android.os.Handler
import android.os.IBinder
import android.os.Looper
import android.view.View
import android.widget.ImageView
import android.widget.TextView
import androidx.appcompat.widget.Toolbar
import de.vps.fame.BuildConfig
import de.vps.fame.R
import de.vps.fame.base.AppApplication
import de.vps.fame.service.NetworkService
import de.vps.fame.tool.FameConst
import de.vps.fame.tool.Logger
import de.vps.fame.tool.PlaceJsonParser
import de.vps.fame.view.fragment.MainFragment
import java.io.BufferedReader
import java.io.File

class MainActivity : BaseActivity() {

    var mtoolbar: Toolbar? = null
    private lateinit var mService: NetworkService
    private var mBound: Boolean = false

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        mtoolbar = findViewById(R.id.toolbar)

        setSupportActionBar(mtoolbar)

        fragmentTag = "#"+R.id.main_frag_container

        writeAssetToFile()
        createData()

        val mainFrag2 = MainFragment()
        val state = mainFrag2.pushFragment(this,R.id.main_frag_container)
        Logger.w("fragment state: ${state.name} - ${mainFrag2.tag}")

    }

    fun setTitel(title: String) {
        findViewById<TextView>(R.id.toolbar_title).text = title
        findViewById<ImageView>(R.id.toolbar_image).visibility = View.INVISIBLE
    }

    private fun writeAssetToFile() {

        // write json
        val file = File(filesDir.absolutePath, FameConst.FILENAME_PLACE_JSON)
        if (!file.exists()) {
            Logger.e("write file")
            val json = PlaceJsonParser.loadMapData(this)
            this.openFileOutput(FameConst.FILENAME_PLACE_JSON, Context.MODE_PRIVATE).use {
                it.write(json.toByteArray())
            }
        }

        val file1 = File(filesDir.absolutePath, FameConst.FILENAME_TIME_JSON)
        if (!file1.exists()) {
            val dateString = BuildConfig.VERSION_JSON_TIME
            this.openFileOutput(FameConst.FILENAME_TIME_JSON, Context.MODE_PRIVATE).use {
                it.write(dateString.toByteArray())
            }
        }
    }

    private fun createData() {

        val parser = PlaceJsonParser()
        val inputString = this.openFileInput(FameConst.FILENAME_PLACE_JSON).bufferedReader().use(BufferedReader::readText)
        val list = parser.parseJson(inputString)

        AppApplication.poiList = list
        AppApplication.rubrikList = parser.getRubrikList(list)
        AppApplication.emergencyNumberList = parser.emergencyCallList
    }

    override fun onResume() {
        super.onResume()

        Intent(this, NetworkService::class.java).also { intent ->
            bindService(intent, connection, Context.BIND_AUTO_CREATE)
        }
    }

    override fun onPause() {
        super.onPause()
        mBound = false
        unbindService(connection)
    }
/*
    fun sendMessage(msg: String) {
        Logger.e("########################################!!")

        if (mBound) {
            mService.startNetworkService(Handler(Looper.getMainLooper()) {
                val i = it.obj as String
                Logger.w("msg: $i")
                true
            })
        }
    }
*/
    private val connection = object : ServiceConnection {

        override fun onServiceConnected(className: ComponentName, service: IBinder) {

            Logger.e("### connected")

            val binder = service as NetworkService.LocalBinder
            mService = binder.getService()
            mService.startNetworkService(Handler(Looper.getMainLooper()))

            mBound = true
        }

        override fun onServiceDisconnected(arg0: ComponentName) {
            mBound = false
        }
    }

}

