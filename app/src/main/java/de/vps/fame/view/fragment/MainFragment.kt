package de.vps.fame.view.fragment

import android.content.Intent
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import de.vps.fame.R
import de.vps.fame.tool.FameConst
import de.vps.fame.tool.Logger
import de.vps.fame.view.activity.MainActivity
import de.vps.fame.view.activity.MapActivity


class MainFragment : BaseFragment() {

    private lateinit var rootView: View

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {

        rootView = inflater.inflate(R.layout.frag_main, container, false)

        rootView.findViewById<View>(R.id.frag_main_map_button)
            .setOnClickListener { pressMap() }
        rootView.findViewById<View>(R.id.frag_main_list_button)
            .setOnClickListener { pressList() }
        rootView.findViewById<View>(R.id.frag_main_falken_button)
            .setOnClickListener { pressFalken() }
        rootView.findViewById<View>(R.id.frag_main_emergency_call_button)
            .setOnClickListener { pressEmergencyCall() }


        (requireActivity() as MainActivity).setTitel(getString(R.string.app_name))

        return rootView
    }

    private fun pressMap() {
        Intent(activity,MapActivity::class.java)
            .also { intent ->
                intent.putExtra(FameConst.PLACE_STATE_KEY, FameConst.PLACE_STATE_ALL)
                startActivity(intent)
            }
    }

    private fun pressList() {
        val rubrikListFragment = RubrikListFragment()
        val state = rubrikListFragment.pushFragment(activity!!,R.id.main_frag_container)
        Logger.w("fragment state: ${state.name} - ${rubrikListFragment.tag}")
    }

    private fun pressFalken() {
        val inprintFragment = InprintFragment()
        val state = inprintFragment.pushFragment(activity!!,R.id.main_frag_container)
        Logger.w("fragment state: ${state.name} - ${inprintFragment.tag}")

       // (requireActivity() as MainActivity).sendMessage("TEST ME")
    }

    private fun pressEmergencyCall() {
        val callFragment = CallFragment()
        val state = callFragment.pushFragment(activity!!,R.id.main_frag_container)
        Logger.w("fragment state: ${state.name} - ${callFragment.tag}")
    }

    override fun onBackPressed() {
        super.onBackPressed()
        requireActivity().finish()
    }
}