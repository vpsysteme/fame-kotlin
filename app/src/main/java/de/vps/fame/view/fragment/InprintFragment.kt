package de.vps.fame.view.fragment

import android.os.Build
import android.os.Bundle
import android.text.Html
import android.text.method.LinkMovementMethod
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import de.vps.fame.R
import de.vps.fame.tool.Logger
import de.vps.fame.tool.Tool
import de.vps.fame.view.activity.MainActivity


class InprintFragment : BaseFragment() {

    private lateinit var rootView: View
    private lateinit var mImprintText: TextView

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {

        rootView = inflater.inflate(R.layout.frag_imprint, container, false)

        rootView.isFocusableInTouchMode = true
        rootView.requestFocus()

        initGUI()

        return rootView
    }

    private fun initGUI() {

        (requireActivity() as MainActivity).setTitel(getString(R.string.frag_main_button_falken))

        mImprintText = rootView.findViewById(R.id.frag_imprint_text)
       // mImprintText.setMovementMethod( ScrollingMovementMethod())

        val html = getString(R.string.imprint) + Tool.getVersionInfo() + getString(R.string.datenschutz)

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
            mImprintText.text = Html.fromHtml(html, Html.FROM_HTML_MODE_LEGACY)
        } else {
            mImprintText.text = Html.fromHtml(html)
        }

        mImprintText.movementMethod = LinkMovementMethod.getInstance()
    }

    override fun onBackPressed() {
        super.onBackPressed()

        val mainFrag2 = MainFragment()
        val state = mainFrag2.pushFragment(activity!!,R.id.main_frag_container)
        Logger.w("fragment state: ${state.name} - ${mainFrag2.tag}")
    }


}