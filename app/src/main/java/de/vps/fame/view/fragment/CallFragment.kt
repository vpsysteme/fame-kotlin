package de.vps.fame.view.fragment

import android.content.Context
import android.content.pm.PackageManager
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.*
import androidx.fragment.app.FragmentTransaction
import de.vps.fame.R
import de.vps.fame.base.AppApplication
import de.vps.fame.model.Place
import de.vps.fame.tool.FameConst
import de.vps.fame.tool.Logger
import de.vps.fame.tool.PhoneTool
import de.vps.fame.view.activity.MainActivity
import de.vps.fame.view.dialog.AlertDialogFragment
import de.vps.fame.view.dialog.CallDialogFragment
import java.util.*

class CallFragment: BaseFragment() {

    private lateinit var rootView: View

    private lateinit var mListView: ListView

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {

        rootView = inflater.inflate(R.layout.frag_call, container, false)

        initGUI()
        initData()

        return rootView
    }

    private fun initGUI() {

        (requireActivity() as MainActivity).setTitel(getString(R.string.frag_main_call_title))

        mListView = rootView.findViewById(R.id.frag_call_list_view)
        val footerView = View(activity)
        footerView.layoutParams = AbsListView.LayoutParams(
            AbsListView.LayoutParams.MATCH_PARENT,
            1
        )
        mListView.addFooterView(footerView)
    }

    private fun initData() {
        val list = AppApplication.emergencyNumberList

        if (list.size > 0) {
            mListView.adapter =
                CallListAdapter(context,list)
        } else {
            Logger.e("mapDataList is null")
        }
    }

    /**
     * call phone number - send a intent to the phone app
     * or show a error dialog
     *
     * @param phoneNumber phone number
     */
    private fun callPhoneNumber(phoneNumber: String) {

        val newNumber = PhoneTool.getPhoneNumber(phoneNumber)
        if (checkPhoneSupport()) {
            if (FameConst.CAN_PHONE_CALL) {
                val mag: String = getString(R.string.msg_phone_call_one) +
                        "\n\n" + phoneNumber.replace(" ", "").replace("/", "")
                    .trim { it <= ' ' } + "\n\n" + getString(R.string.msg_phone_call_two)
                val ft: FragmentTransaction =
                    activity!!.supportFragmentManager.beginTransaction()
                CallDialogFragment.newInstance(mag, newNumber)
                    .show(ft, CallFragment::class.java.simpleName)
            } else {
                Toast.makeText(activity, "Tel: $newNumber", Toast.LENGTH_SHORT)
                    .show() //For debug
            }
        } else {
            try {
                val ft: FragmentTransaction =
                    activity!!.supportFragmentManager.beginTransaction()
                AlertDialogFragment.newInstance(
                    getString(R.string.msg_no_phone_device)
                ).show(ft, CallFragment::class.java.simpleName)
            } catch (ignored: Exception) {
            }
        }
    }

    private fun checkPhoneSupport(): Boolean {
        val pm = activity!!.packageManager
        return pm.hasSystemFeature(PackageManager.FEATURE_TELEPHONY)
    }

    override fun onBackPressed() {
        super.onBackPressed()

        val mainFrag = MainFragment()
        val state = mainFrag.pushFragment(activity!!,R.id.main_frag_container)
        Logger.w("fragment state: ${state.name} - ${mainFrag.tag}")
    }

    /////////////////////////////////////////////////////////////////////////////////
    // Adapter for list view
    /////////////////////////////////////////////////////////////////////////////////
    /**
     * A Adapter for the call data
     */
    inner class CallListAdapter(
        private val mContext: Context?,
        dataList: ArrayList<Place>
    ) : ArrayAdapter<Place>(mContext!!, R.layout.item_frag_call_list, dataList) {

        private var mDataList: ArrayList<Place> = dataList

        override fun getCount(): Int {
            return mDataList.size
        }

        override fun getView(position: Int, convertView: View?,parent: ViewGroup): View {

            val inflater = mContext!!.getSystemService(Context.LAYOUT_INFLATER_SERVICE) as LayoutInflater
            //TODO use ViewHolder
            val view = inflater.inflate(R.layout.item_frag_call_list, parent, false)
            val nameTextView = view.findViewById<TextView>(R.id.item_call_name)
            val phoneTextView = view.findViewById<TextView>(R.id.item_call_number)
            val mapData = mDataList[position]
            var teflon = mapData.telefon
            nameTextView.text = mapData.name

            if (teflon.contains("oder")) {
                teflon = teflon.substring(0, teflon.indexOf("oder"))
            }
            phoneTextView.text = teflon.trim { it <= ' ' }
            view.setOnClickListener { callPhoneNumber(mapData.telefon) }
            return view
        }

    }


}