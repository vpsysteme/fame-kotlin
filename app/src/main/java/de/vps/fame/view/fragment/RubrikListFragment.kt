package de.vps.fame.view.fragment

import android.annotation.SuppressLint
import android.content.Context
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.BaseExpandableListAdapter
import android.widget.ExpandableListView
import android.widget.ImageView
import android.widget.TextView
import de.vps.fame.R
import de.vps.fame.base.AppApplication
import de.vps.fame.model.Rubrik
import de.vps.fame.tool.Logger
import de.vps.fame.view.activity.MainActivity
import java.util.*

class RubrikListFragment : BaseFragment(){

    private lateinit var rootView: View
    private lateinit var mListView : ExpandableListView

    var expandedId = -1

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {

        rootView = inflater.inflate(R.layout.frag_rubrik_list, container, false)

        initGUI()

        return rootView
    }

    private fun initGUI() {

        (requireActivity() as MainActivity).setTitel(getString(R.string.frag_main_list_title))


        val bar = (requireActivity() as MainActivity).mtoolbar

        bar?.findViewById<View>(R.id.toolbar_image)?.visibility = View.VISIBLE
        (bar?.findViewById<ImageView>(R.id.toolbar_image))?.setImageResource(R.drawable.ic_search)

        bar?.findViewById<View>(R.id.toolbar_image_touch)
            ?.setOnClickListener {
                val searchListFragment = SearchListFragment()
                val state = searchListFragment.pushFragment(activity!!,R.id.main_frag_container)
                Logger.w("fragment state: ${state.name} - ${searchListFragment.tag}")
            }



        mListView = rootView.findViewById(R.id.frag_rubrik_expandable_list)
        mListView.setGroupIndicator(null)

        val rubrikList = AppApplication.rubrikList

        mListView.setAdapter(RubrikListAdapter(context, rubrikList))

        if (expandedId > -1) {
            mListView.expandGroup(expandedId)
        }

        //for (i in rubrikList.indices) {
        //    mListView.expandGroup(i)
        //}

        mListView.setOnChildClickListener { _, _, groupPosition, childPosition, _ ->

            val rubrik = rubrikList[groupPosition]
            val themenListFragment = ThemenListFragment()
            themenListFragment.setRubrik(rubrik, rubrik.themenList[childPosition],groupPosition)
            val state = themenListFragment.pushFragment(activity!!,R.id.main_frag_container)
            Logger.w("fragment state: ${state.name} - ${themenListFragment.tag}")

            true
        }

        mListView.setOnGroupClickListener { parent, v, groupPosition, id ->
            /*
                if (mRubrikList.get(groupPosition).themaList.size() < 1) {
                    val group: Int = mRubrikList.get(groupPosition).getGroupPos()
                    mFragmentController.replacePlacesListFragment(
                        R.id.act_main_frame_layout, group, 0,
                        Tool.getName(mRubrikList.get(groupPosition).name)
                    )
                    return@OnGroupClickListener true
                }
                 */
            false
        }

    }

    override fun onBackPressed() {
        super.onBackPressed()

        val mainFrag = MainFragment()
        val state = mainFrag.pushFragment(activity!!,R.id.main_frag_container)
        Logger.w("fragment state: ${state.name} - ${mainFrag.tag}")
    }


    /////////////////////////////////////////////////////////////////////////////////
    // Adapter for list view
    /////////////////////////////////////////////////////////////////////////////////

    //private var rubriks: HashMap<String, HashSet<String>>

    private class RubrikListAdapter(
        private val mContext: Context?,
        rubrikList: ArrayList<Rubrik>
    ) :
        BaseExpandableListAdapter() {
        private val mRubrikList: ArrayList<Rubrik> = rubrikList
        override fun getGroupCount(): Int {
            return mRubrikList.size
        }

        override fun getChildrenCount(groupPosition: Int): Int {
            return mRubrikList[groupPosition].themenList.size
        }

        override fun getGroup(groupPosition: Int): Any {
            return mRubrikList[groupPosition]
        }

        override fun getChild(groupPosition: Int, childPosition: Int): Any {
            return mRubrikList[groupPosition].themenList[childPosition]
        }

        override fun getGroupId(groupPosition: Int): Long {
            return groupPosition.toLong()
        }

        override fun getChildId(groupPosition: Int, childPosition: Int): Long {
            return childPosition.toLong()
        }

        override fun hasStableIds(): Boolean {
            return false
        }

        @SuppressLint("InflateParams")
        override fun getGroupView(
            groupPosition: Int,
            isExpanded: Boolean,
            convertView: View?,
            parent: ViewGroup
        ): View {

            val title: String = mRubrikList[groupPosition].name
            val conView = if (convertView == null) {
                val layoutInflater =
                    mContext?.getSystemService(Context.LAYOUT_INFLATER_SERVICE) as LayoutInflater
                 layoutInflater.inflate(R.layout.item_rubrik_group_list, null)
            } else {
                convertView
            }
            val textView =
                conView.findViewById<TextView>(R.id.it_rubrik_header)
            textView.text = title
            val indicatorView =
                conView.findViewById<ImageView>(R.id.it_rubrik_list_indicator)
            if (getChildrenCount(groupPosition) == 0) {
                indicatorView.visibility = View.INVISIBLE
            } else {
                indicatorView.visibility = View.VISIBLE
                indicatorView.setImageResource(if (isExpanded) R.drawable.ic_group_open else R.drawable.ic_group_close)
            }
            return conView
        }

        @SuppressLint("InflateParams")
        override fun getChildView(
            groupPosition: Int,
            childPosition: Int,
            isLastChild: Boolean,
            convertView: View?,
            parent: ViewGroup
        ): View {

            val title: String = mRubrikList[groupPosition].themenList[childPosition]

            val conView = if (convertView == null) {
                val layoutInflater =
                    mContext?.getSystemService(Context.LAYOUT_INFLATER_SERVICE) as LayoutInflater
                layoutInflater.inflate(R.layout.item_rubrik_thema_list, null)
            } else {
                convertView
            }
            val textView = conView.findViewById<TextView>(R.id.item_rubrik_thema)
            textView.text = title
            return conView
        }

        override fun isChildSelectable(
            groupPosition: Int,
            childPosition: Int
        ): Boolean {
            return true
        }

    }
}