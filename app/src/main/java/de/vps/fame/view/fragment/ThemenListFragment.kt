package de.vps.fame.view.fragment

import android.Manifest
import android.content.Context
import android.content.Intent
import android.content.pm.PackageManager
import android.location.Location
import android.os.Bundle
import android.os.Looper
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ArrayAdapter
import android.widget.Filterable
import android.widget.ListView
import android.widget.TextView
import androidx.core.app.ActivityCompat
import com.google.android.gms.common.ConnectionResult
import com.google.android.gms.common.api.GoogleApiClient
import com.google.android.gms.common.api.GoogleApiClient.ConnectionCallbacks
import com.google.android.gms.common.api.GoogleApiClient.OnConnectionFailedListener
import com.google.android.gms.location.*
import com.google.android.material.floatingactionbutton.FloatingActionButton
import de.vps.fame.R
import de.vps.fame.base.AppApplication
import de.vps.fame.model.Place
import de.vps.fame.model.Rubrik
import de.vps.fame.tool.FameConst
import de.vps.fame.tool.Logger
import de.vps.fame.tool.MapTool
import de.vps.fame.view.activity.MainActivity
import de.vps.fame.view.activity.MapActivity
import java.util.*

class ThemenListFragment : BaseFragment(), ConnectionCallbacks,
    OnConnectionFailedListener {

    private var fusedLocClient: FusedLocationProviderClient? = null
    private var mGoogleApiClient: GoogleApiClient? = null
    private var mCurrentLocation: Location? = null

    private lateinit var mListView: ListView
    private lateinit var rootView: View

    private lateinit var rubrik: Rubrik
    private lateinit var thema: String
    private var expandetId: Int = -1

    private lateinit var poiDataList: ArrayList<Place>


    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {

        rootView = inflater.inflate(R.layout.frag_themen_list, container, false)

        initGUI()

        return rootView
    }

    private fun initGUI() {

        mListView = rootView.findViewById(R.id.frag_places_list_view)

        (requireActivity() as MainActivity).setTitel(thema)

        val mFloatingActionButton: FloatingActionButton =
            rootView.findViewById(R.id.floating_button)

        mFloatingActionButton.alpha = 0.4f
        mFloatingActionButton.visibility = View.VISIBLE
        mFloatingActionButton.setOnClickListener {
            Intent(requireContext(), MapActivity::class.java)
                .also { intent ->
                    intent.putExtra(FameConst.PLACE_STATE_KEY, FameConst.PLACE_STATE_THEMA)
                    intent.putExtra(FameConst.RUBRIK_KEY, rubrik.name)
                    intent.putExtra(FameConst.THEMA_KEY, thema)
                    startActivity(intent)
                }
        }

        poiDataList = getPoiList()

        initLocationRequest()
    }

    fun setRubrik(rubrik: Rubrik, thema: String, expandetId: Int) {
        this.rubrik = rubrik
        this.thema = thema
        this.expandetId = expandetId
    }

    override fun onBackPressed() {
        super.onBackPressed()

        val mainFrag = RubrikListFragment()
        mainFrag.expandedId = expandetId
        val state = mainFrag.pushFragment(activity!!, R.id.main_frag_container)
        Logger.w("fragment state: ${state.name} - ${mainFrag.tag}")
    }

    private fun getPoiList(): ArrayList<Place> {

        val list = AppApplication.poiList

        return list.filter {
            it.rubrik == rubrik.name && it.thema == thema
        } as ArrayList<Place>
    }

    private fun refreshData(flag: Boolean) {

        val list = getPoiList()

        if (mCurrentLocation != null) {
            list.sortWith(Comparator { data1, data2 ->
                val distance1 = MapTool.distance(data1, mCurrentLocation!!)
                val distance2 = MapTool.distance(data2, mCurrentLocation!!)
                distance1.compareTo(distance2)
            })
        }

        val adapter = PlaceListAdapter(requireContext(), list)
        mListView.adapter = adapter

        try {
            adapter.notifyDataSetChanged()
        } catch (e: Exception) {
            Logger.e("Can not change data in adapter", e)
        }

    }

    /////////////////////////////////////////////////////////////////////////////////
    //  Localisation Google API
    /////////////////////////////////////////////////////////////////////////////////

    private fun initLocationRequest() {

        refreshData(false)

        initGoogleApi()

        fusedLocClient = LocationServices.getFusedLocationProviderClient(requireActivity())

        if (checkPermission()) {
            requestPermissions(
                arrayOf(
                    Manifest.permission.ACCESS_COARSE_LOCATION,
                    Manifest.permission.ACCESS_FINE_LOCATION
                ),
                FameConst.PERMISSIONS_REQUEST_ACCESS_LOCATION
            )
        }
    }

    /**
     * init google api and connect with google apk
     */
    @Synchronized
    private fun initGoogleApi() {
        mGoogleApiClient = GoogleApiClient.Builder(activity!!)
            .addConnectionCallbacks(this)
            .addOnConnectionFailedListener(this)
            .addApi(LocationServices.API)
            .build()
        mGoogleApiClient?.connect()
    }

    private fun checkPermission(): Boolean {
        return ActivityCompat.checkSelfPermission(
            context!!,
            Manifest.permission.ACCESS_FINE_LOCATION
        ) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(
            context!!, Manifest.permission.ACCESS_COARSE_LOCATION
        ) != PackageManager.PERMISSION_GRANTED
    }

    override fun onStart() {
        super.onStart()
        if (mGoogleApiClient != null) {
            mGoogleApiClient!!.connect()
            Logger.e("client connect")
        }
    }

    override fun onStop() {
        mGoogleApiClient!!.disconnect()
        stopLocationUpdates()
        super.onStop()
    }

    private fun startLocationUpdates() {

        if (checkPermission()) return

        val locationRequest = LocationRequest()
        locationRequest.interval = FameConst.LOCATION_UPDATE_INTERVAL
        locationRequest.fastestInterval = FameConst.LOCATION_FASTEST_UPDATE_INTERVAL
        locationRequest.priority = LocationRequest.PRIORITY_HIGH_ACCURACY

        fusedLocClient!!.requestLocationUpdates(
            locationRequest,
            mLocationCallback,
            Looper.myLooper()
        )
    }

    private fun stopLocationUpdates() {
        fusedLocClient!!.removeLocationUpdates(mLocationCallback)
    }

    private val mLocationCallback: LocationCallback = object : LocationCallback() {
        override fun onLocationResult(locationResult: LocationResult) {
            Logger.e("refresh Data")

            if (mCurrentLocation != null) {

                if (locationResult.lastLocation.distanceTo(mCurrentLocation) > 5) {
                    mCurrentLocation = locationResult.lastLocation
                    refreshData(true)
                }
            } else {
                mCurrentLocation = locationResult.lastLocation
                refreshData(true)
            }
        }
    }

    override fun onConnected(bundle: Bundle?) {
        startLocationUpdates()
    }

    override fun onConnectionSuspended(i: Int) {
        mGoogleApiClient!!.connect()
    }

    override fun onConnectionFailed(p0: ConnectionResult) {
        Logger.e("we have a problem with the connection")

        refreshData(false)
    }

    override fun onRequestPermissionsResult(
        requestCode: Int,
        permissions: Array<String>,
        grantResults: IntArray
    ) {
        when (requestCode) {
            FameConst.PERMISSIONS_REQUEST_ACCESS_LOCATION -> if (grantResults.isNotEmpty() && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                Logger.i("PERMISSION_GRANTED")
                startLocationUpdates()
            }
        }
    }


    /////////////////////////////////////////////////////////////////////////////////
    // Adapter
    /////////////////////////////////////////////////////////////////////////////////

    /**
     * A Adapter for the poi
     * The Adapter is searchable
     */
    inner class PlaceListAdapter(
        private val mContext: Context,
        placeList: ArrayList<Place>
    ) :
        ArrayAdapter<Place>(mContext, R.layout.item_places_data_list, placeList),
        Filterable {
        private var dataList: ArrayList<Place> = placeList

        override fun getCount(): Int {
            return dataList.size
        }

        override fun getView(position: Int, convertView: View?, parent: ViewGroup): View {

            val inflater =
                mContext.getSystemService(Context.LAYOUT_INFLATER_SERVICE) as LayoutInflater

            val conView =
                convertView ?: inflater.inflate(R.layout.item_places_data_list, parent, false)

            val poi = dataList[position]

            conView.setOnClickListener {
                Intent(requireContext(), MapActivity::class.java)
                    .also { intent ->
                        intent.putExtra(FameConst.PLACE_STATE_KEY, FameConst.PLACE_STATE_POI)
                        intent.putExtra(FameConst.POI_KEY, dataList[position].id)
                        startActivity(intent)
                    }
            }
            conView.findViewById<TextView>(R.id.item_places_name).text = poi.name
            val distanceTextView = conView.findViewById<TextView>(R.id.item_places_distance)
            if (mCurrentLocation == null) {
                distanceTextView.text = ""
                return conView
            }
            distanceTextView.text = MapTool.getDistanceText(poi, mCurrentLocation!!)

            return conView
        }
    }

}
