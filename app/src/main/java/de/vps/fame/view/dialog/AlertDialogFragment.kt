package de.vps.fame.view.dialog

import android.os.Bundle
import android.view.*
import android.widget.TextView
import de.vps.fame.R
import de.vps.fame.tool.FameConst

class AlertDialogFragment(timeBreak: Int) : BaseDialogFragment(timeBreak) {

    companion object {

        private const val TITEL = "titel"

        fun newInstance(title: String): AlertDialogFragment {
            val fragment = AlertDialogFragment(FameConst.DIALOG_TIMER_BREAK)
            fragment.arguments!!.putString(TITEL, title)
            return fragment
        }
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        initDialogGUI()

        val title = this.arguments!!.getString(TITEL)

        val view = inflater.inflate(R.layout.dialogfrag_alert, container, false)

        view.findViewById<TextView>(R.id.dialog_alert_text).text = title

        view.findViewById<View>(R.id.dialog_alert_ok_button)
            .setOnClickListener { dismiss() }

        return view
    }

    private fun initDialogGUI() {
        try {
            dialog!!.window!!.requestFeature(Window.FEATURE_NO_TITLE)
        } catch (ignore: NullPointerException) {}

        //		getDialog().getWindow().getDecorView().setSystemUiVisibility(View.GONE);

        dialog!!.window!!.setGravity(Gravity.CENTER_HORIZONTAL or Gravity.CENTER_VERTICAL)
        val lp = dialog!!.window!!.attributes
        lp.dimAmount = 0.8f
        dialog!!.window!!.attributes = lp
        dialog!!.window!!.addFlags(WindowManager.LayoutParams.FLAG_DIM_BEHIND)

        dialog!!.setCanceledOnTouchOutside(false)
    }

}