package de.vps.fame.view.fragment


import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentActivity
import androidx.fragment.app.FragmentManager
import androidx.fragment.app.FragmentTransaction

open class BaseFragment: Fragment() {

    /**
     * Add or replace the Fragment in the ViewGroup with the resId
     * DANGER:
     * In the Method Activity.onCreate(...) the function call everytime fm.add. Not fm.replace.
     * Please: not use in onCreate(...)
     */
    fun pushFragment(activity: FragmentActivity, resId: Int): FragmentState {
        val tags = "#$resId"

        val fm = activity.supportFragmentManager
        val ft: FragmentTransaction = fm.beginTransaction()

        val oldFrag = fm.findFragmentByTag(tags)
        val state: FragmentState = if (oldFrag != null) {
            fm.popBackStackImmediate(
                tags,
                FragmentManager.POP_BACK_STACK_INCLUSIVE
            )
            ft.replace(resId, this, tags)
            FragmentState.REPLACE
        } else {
            ft.add(resId, this, tags)
            FragmentState.ADD
        }
        ft.setTransition(FragmentTransaction.TRANSIT_NONE)

        ft.addToBackStack(tags)
        ft.commit()
        return state
    }

    open fun onBackPressed() {}
}

enum class FragmentState {
    ADD,
    REPLACE
}