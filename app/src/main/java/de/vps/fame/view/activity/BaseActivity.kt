package de.vps.fame.view.activity

import android.view.KeyEvent
import androidx.appcompat.app.AppCompatActivity
import de.vps.fame.view.fragment.BaseFragment

open class BaseActivity : AppCompatActivity() {

    var fragmentTag = ""

    /**
     * Called when a key was pressed down.
     */
    override fun onKeyDown(keyCode: Int, event: KeyEvent?): Boolean {

        // pressed Back Button
        if (keyCode == KeyEvent.KEYCODE_BACK) {
            if (fragmentTag.isNotEmpty()) {

                val oldFrag = supportFragmentManager.findFragmentByTag(fragmentTag)
                if (oldFrag != null) {

                    if (oldFrag is BaseFragment) {
                        oldFrag.onBackPressed()
                        return false
                    }
                }
            }

        }
        return super.onKeyDown(keyCode, event)
    }
}