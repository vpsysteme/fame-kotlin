package de.vps.fame.view.dialog


import android.os.Bundle
import androidx.fragment.app.DialogFragment
import androidx.fragment.app.FragmentTransaction
import de.vps.fame.tool.Logger

open class BaseDialogFragment(): DialogFragment() {

    constructor(timeBreak: Int) :this() {
        val args = Bundle()
        args.putInt(TIME_BREAK, timeBreak)
        arguments = args
    }

    companion object {
        private const val TIME_BREAK ="timeBreak"
    }

    override fun show(transaction: FragmentTransaction, tag: String?): Int {
        val time = arguments!!.getInt("TIME_BREAK")

        Logger.d("showing dialog")
        if (time > 0) {
            CloseThread(this).start()
        }

        return super.show(transaction, tag)
    }

    /**
     * Close the dialog after some time.
     */
    private class CloseThread(private val dialog: DialogFragment?): Thread() {
        override fun run() {
            try {
                val time: Int = dialog!!.arguments!!.getInt("TIME_BREAK", 1000*30)
                sleep(time.toLong())
                dialog.dismiss()
                Logger.d("dialog dismiss()")
            } catch (ignored: Exception) {
            }
        }

    }
}