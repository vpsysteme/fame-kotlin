package de.vps.fame.viewmodel

import androidx.lifecycle.ViewModel
import de.vps.fame.tool.Logger

class PlaceModel : ViewModel() {

    init {
        Logger.i("PlaceModel")
    }

    override fun onCleared() {
        super.onCleared()
        Logger.i("PlaceModel")
    }
}
