package de.vps.fame.service

import android.app.Service
import android.content.Context
import android.content.Intent
import android.os.Binder
import android.os.Handler
import android.os.IBinder
import android.os.Looper
import android.widget.Toast
import com.android.volley.*
import de.vps.fame.base.AppApplication
import de.vps.fame.network.Network
import de.vps.fame.tool.FameConst
import de.vps.fame.tool.Logger
import de.vps.fame.tool.PlaceJsonParser
import de.vps.fame.tool.Tool
import org.json.JSONObject
import java.io.BufferedReader
import java.util.*
import kotlin.concurrent.timerTask


class NetworkService : Service() {

    companion object {
        const val CON_VERSION = "version"
        const val CON_DATA = "data"
    }

    private val binder = LocalBinder()
    private var mHandler: Handler? = null
    private var network: Network? = null

    private var timer: Timer? = null

    override fun onBind(intent: Intent?): IBinder? {
        return binder
    }

    fun startNetworkService(handler: Handler) {
        mHandler = handler
        timer = Timer()
        timer?.schedule(timerTask {
            checkVersionTime()
        }, 1000, 1000 * 60 * 60)
    }

    override fun onDestroy() {
        super.onDestroy()
        network?.destroyConnection(CON_DATA)
        network?.destroyConnection(CON_VERSION)
        timer?.cancel()
        timer = null
    }

    /**
     * Die Funktion erzeugt überprüft, ob die Version der Daten (POI)
     */
    private fun checkVersionTime() {
        // load Date
        val dateString = this.openFileInput(FameConst.FILENAME_TIME_JSON).bufferedReader().use(
            BufferedReader::readText)

        //val dateString = Tool.parseDateToString(Date()) // TO UTC

        Network(applicationContext)
            .setUrl(FameConst.URL_SERVER + FameConst.QUERY_TIME_1 + dateString + FameConst.QUERY_TIME_2)
            .setReponseListener(Response.Listener { response ->
                Logger.e(response)
                val result = parseVersionJson(response)
                if (result) {
                    loadData()
                } else {
                    Logger.d("data is up to date")
                }
            })
            .setErrorListener(Response.ErrorListener { error ->
                logError(error)
            })
            .sendRequest(CON_VERSION)
    }

    /**
     * Die FunKtion lädt die POI (Json) vom CMS. Diese werden abgespeichert, und geparst.
     * Da die id enindeutig sind, kann dies in Echzeit passieren.
     */
    private fun loadData() {
        Logger.i("load data")
        Network(applicationContext)
            .setUrl(FameConst.URL_SERVER + FameConst.QUERY_POI)
            .setReponseListener(Response.Listener { response ->

                val parser = PlaceJsonParser()
                val list = parser.parseJson(response)

                if (list.size >0) {

                    AppApplication.poiList = list
                    AppApplication.rubrikList = parser.getRubrikList(list)
                    AppApplication.emergencyNumberList = parser.emergencyCallList

                    this.openFileOutput(FameConst.FILENAME_PLACE_JSON, Context.MODE_PRIVATE).use {
                        it.write(response.toByteArray())
                    }

                    val dateString = Tool.parseDateToString(Date())
                    this.openFileOutput(FameConst.FILENAME_TIME_JSON, Context.MODE_PRIVATE).use {
                        it.write(dateString?.toByteArray())
                    }

                    Logger.i("load data finish")

                    if (BuildConfig.DEBUG) {
                        val handler = Handler(Looper.getMainLooper())
                        handler.post {
                            Toast.makeText(
                                this@NetworkService.applicationContext,
                                "Daten wurden aktualisiert.",
                                Toast.LENGTH_SHORT
                            ).show()
                        }
                    }
                } else {
                    Logger.e("Data can not cast to poiList")
                }

                //val msg =
                //Message.obtain(null,1,  0, 0 ,"Test ok")
                //mHandler?.sendMessage(msg)
            })
            .setErrorListener(Response.ErrorListener {error -> logError(error)})
            .sendRequest(CON_DATA)
    }

    inner class LocalBinder : Binder() {
        // Return this instance of LocalService so clients can call public methods
        fun getService(): NetworkService = this@NetworkService
    }


    private fun parseVersionJson(json: String): Boolean {
        return try {
            JSONObject(json).getJSONObject("result").getBoolean("has_updates")
        } catch (e: Exception) {
            Logger.e("Exception parse json", e)
            false
        }
    }

    private fun logError(error: VolleyError) {
        when (error) {
            is TimeoutError -> Logger.e("Network time out")
            is ServerError -> Logger.e("Server error")
            is NetworkError -> Logger.e("Network error")
            is ParseError -> Logger.e("Parse Error")
            else -> Logger.e("Unkonwing Error")
        }
    }
}