package de.vps.fame.model

data class Place (val id: String) {
    var rubrik: String = ""
    var thema: String = ""
    var name: String = ""
    var zusatz: String = ""
    var strasse: String = ""
    var plz: String = ""
    var ort: String = ""
    var ansprechpartner: String = ""
    var telefon: String = ""
    var fax: String = ""
    var internet: String = ""
    var email: String = ""
    var statviertel: String = ""
    var zusatzinfo: String = ""
    var sozialraum: String = ""
    var icon: String = ""
    var lat = 0.0
    var lon = 0.0
}
