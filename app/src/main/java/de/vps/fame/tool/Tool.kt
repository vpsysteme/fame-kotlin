package de.vps.fame.tool

import android.content.Context
import de.vps.fame.BuildConfig
import java.text.ParseException
import java.text.SimpleDateFormat
import java.util.*

object Tool {

    private const val FORMATE_DATE = "yyyy-MM-dd'T'HH:mm:ss'Z'"

    fun getVersionInfo(): String? {
        return BuildConfig.VERSION_NAME // +" ("+BuildConfig.VERSION_CODE+")";
    }

    /**
     * Wird für die Abfrage des Standes der Daten auf dem Server benötigt
     */
    fun parseStringToDate(time: String): Date? {
        val input = SimpleDateFormat(FORMATE_DATE, Locale.ENGLISH)
        input.timeZone = TimeZone.getTimeZone("UTC")
        return try {
            input.parse(time)
        } catch (e: ParseException) {
            null
        }
    }

    /**
     * Wird für die Abfrage des Standes der Daten auf dem Server benötigt
     */
    fun parseDateToString(date: Date): String? {
        val output = SimpleDateFormat(FORMATE_DATE, Locale.ENGLISH)
        output.timeZone = TimeZone.getTimeZone("UTC")
        return try {
            output.format(date)
        } catch (e: ParseException) {
            null
        }
    }

    fun pxfromDp( context: Context, dp: Float) :Float {
        return dp * context.resources.displayMetrics.density
    }
}