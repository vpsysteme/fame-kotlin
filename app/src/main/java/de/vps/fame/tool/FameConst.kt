package de.vps.fame.tool

object FameConst {


    const val CAN_PHONE_CALL = false

    //const val JSON_FILE_NAME = "neue json lotse.txt"
    const val JSON_FILE_NAME = "lotse_content.txt"

    const val LAT_BOCHUM = 51.4818445
    const val LON_BOCHUM = 7.2162363

    const val LOCATION_UPDATE_INTERVAL: Long = 20000
    const val LOCATION_FASTEST_UPDATE_INTERVAL: Long = 10000

    const val PERMISSIONS_REQUEST_ACCESS_LOCATION = 1

    const val PLACE_STATE_POI = 0
    const val PLACE_STATE_THEMA = 1
    const val PLACE_STATE_RUBRIK = 2
    const val PLACE_STATE_ALL = -1

    const val PLACE_STATE_KEY = "state"

    const val RUBRIK_KEY = "rubrik"
    const val THEMA_KEY = "thema"
    const val POI_KEY = "poi"

    const val DIALOG_TIMER_BREAK = 1000*30

    const val URL_SERVER = "https://lvoev899.apicdn.sanity.io/v1/data/query/production?query="
    const val QUERY_POI = "{'maps':*[_type=='location']{'id':_id,ansprechpartner,email,fax,id,internet,name,rubrik,telefon,thema,zusatz,'sozialraum': adresse.sozialraum,'statviertel': adresse.statviertel,'strasse': adresse.strasse,'plz': adresse.plz,'ort': adresse.ort,'lat': adresse.coordinates.lat,'lon': adresse.coordinates.lng}}"

    //const val QUERY_POI = "{'maps': *[_type=='location'] {'id': _id,ansprechpartner,email,fax,id,internet,name,plz,rubrik,sozialraum,statviertel,strasse,telefon,thema,zusatz,ort,'lat': coordinates.lat,'lon': coordinates.lng }}"

    const val QUERY_TIME_1 = "{'has_updates': count(*[_updatedAt >= '"//2020-06-23T15:20:48Z']) > 0 }"
    const val QUERY_TIME_2 = "']) > 0 }"

    const val FILENAME_TIME_JSON = "time_json.txt"
    const val FILENAME_PLACE_JSON = "place_json.txt"

}