package de.vps.fame.tool

import android.location.Location
import de.vps.fame.model.Place

object MapTool {

    /**
     * format the discance in meter or km
     *
     * @param meters value in meters
     * @return a string ("x m" or "x.x km")
     */
    private fun formatDist(meters: Double): String {
        return if (meters < 1000)
            "${meters.toInt()} m"
        else
            formatDec(meters / 1000, 2).plus(" km")
    }

    /**
     * format a value in meter to a value in km
     *
     * @param value value
     * @param dec number of decimals
     * @return formatted string
     */
    private fun formatDec(value: Double, dec: Int): String? {

        if (value >= 100 ) return "${value.toInt()}"

        val format = "%.${dec}f"
        return format.format(value)
    }


    fun distance (place: Place, location :Location): Float {

        val distance = FloatArray(1)
        Location.distanceBetween(
            place.lat,
            place.lon,
            location.latitude,
            location.longitude,
            distance
        )

        return distance[0]
    }

    fun getDistanceText(place: Place, location :Location): String {
        return formatDist(distance(place,location).toDouble())
    }

}