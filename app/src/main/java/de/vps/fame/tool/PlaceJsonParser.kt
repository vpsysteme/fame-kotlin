package de.vps.fame.tool

import android.content.Context
import de.vps.fame.model.Place
import de.vps.fame.model.Rubrik
import org.json.JSONArray
import org.json.JSONObject
import java.io.BufferedReader
import java.io.IOException
import java.io.InputStreamReader
import java.nio.charset.StandardCharsets
import kotlin.collections.ArrayList
import kotlin.collections.HashMap
import kotlin.collections.HashSet

class PlaceJsonParser {

    val emergencyCallList = ArrayList<Place>()

    companion object {
        private const val ID = "id"
        private const val RUBRIK = "rubrik"
        private const val THEMA = "thema"
        private const val NAME = "name"
        private const val ZUSATZ = "zusatz"
        private const val STRASSE = "strasse"
        private const val PLZ = "plz"
        private const val ORT = "ort"
        private const val ANSPRECHPARTNER = "ansprechpartner"
        private const val TELEFON = "telefon"
        private const val FAX = "fax"
        private const val INTERNET = "internet"
        private const val EMAIL = "email"
        private const val STADTVIERTEL = "statviertel"
        private const val ZUSATZINFO = "zusatzinfo"
        private const val SOZIALRAUM = "sozialraum"
        private const val ICON = "icon"
        private const val LAT = "lat"
        private const val LON = "lon"
        private const val EMERGENCY_CALL = ">> Notdienste <<"

        fun loadMapData(context: Context): String {

            return try {
                BufferedReader(
                    InputStreamReader(
                        context.assets.open(FameConst.JSON_FILE_NAME),
                        StandardCharsets.UTF_8
                    )
                ).use { reader ->
                    val builder = StringBuilder()
                    var mLine: String?
                    while (reader.readLine().also { mLine = it } != null) {
                        builder.append(mLine)
                    }
                    builder.toString()
                }
            } catch (e: IOException) {
                Logger.e("Can not load json string")
                ""
            }
        }
    }

    fun parseJson(json: String): ArrayList<Place> {
        return try {

            val jsonArray: JSONArray = try {
                JSONObject(json).getJSONObject("result").getJSONArray("maps")
            } catch ( e: Exception) {
                //Logger.e("exception", e)
                JSONObject(json).getJSONArray("maps")
            }
            val size = jsonArray.length()
            val placeList = ArrayList<Place>()

            for (j in 0 until size) {
                val jsonData = jsonArray.getJSONObject(j)
                val place = Place(jsonData.getString(ID))
                place.rubrik = jsonData.optString(RUBRIK,"")
                place.thema = jsonData.optString(THEMA,"")
                place.name = jsonData.optString(NAME,"")
                place.zusatz = jsonData.optString(ZUSATZ,"")

                place.strasse = jsonData.optString(STRASSE,"")
                place.plz = jsonData.optString(PLZ,"")
                place.ort = jsonData.optString(ORT,"")
                place.ansprechpartner = jsonData.optString(ANSPRECHPARTNER,"")
                place.telefon = jsonData.optString(TELEFON,"").replace("\\s?-\\s?".toRegex(), "")
                place.fax = jsonData.optString(FAX,"")
                place.internet = jsonData.optString(INTERNET,"")
                place.email = jsonData.optString(EMAIL,"")
                place.statviertel = jsonData.optString(STADTVIERTEL,"")
                place.zusatzinfo = jsonData.optString(ZUSATZINFO,"")
                place.sozialraum = jsonData.optString(SOZIALRAUM,"")
                place.icon = jsonData.optString(ICON,"")

                place.lat = jsonData.getString(LAT).toDouble()
                place.lon = jsonData.getString(LON).toDouble()
                if (place.rubrik == EMERGENCY_CALL) {
                    emergencyCallList.add(place)
                } else {
                    if (place.thema.isEmpty()) {
                        place.thema = "Allgemein"
                    }
                    place.name = changePlaceName(place)
                    placeList.add(place)
                }
            }
            placeList
        } catch (e: Exception) {
            Logger.e("Exception parse json", e)
            ArrayList()
        }
    }

    fun getRubrikList(list: ArrayList<Place>): ArrayList<Rubrik> {

        val rubriks = HashSet<String>()

        list.forEach { rubriks.add(it.rubrik) }

        val map = HashMap<String, HashSet<String>>()

        rubriks.forEach { map[it] = HashSet() }

        list.forEach {
            val item = map[it.rubrik]
            item!!.add(it.thema)
            map.replace(it.rubrik,item)
        }

        val rubrikList :ArrayList<Rubrik> = ArrayList()

        map.forEach{
            val r = Rubrik(it.key)
            it.value.forEach {t -> r.themenList.add(t) }
            rubrikList.add(r)
        }

        val aList = ArrayList<Rubrik>()

        rubrikList.sortedWith(compareBy {it.name})
            .forEach{
                val r = Rubrik(it.name)
                it.themenList.sortedWith(compareBy { t->t}).forEach{ t-> r.themenList.add(t)}
                aList.add(r)
            }

        aList.forEach{it.themenList.forEach { t -> Logger.w("z ${it.name} - $t") }}

        return aList
    }

    private fun changePlaceName(place: Place): String {
        return if (place.name.isEmpty())
            if (place.rubrik == "Kinderspielplätze") "Kinderspielplatz"
            else "Sonstige"
        else place.name
    }

}