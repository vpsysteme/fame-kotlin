package de.vps.fame.tool

import android.util.Log

/**
 * Log file
 * writen by WDS
 */
object Logger {
    /** error flag  */
    private const val ERROR = true

    /** warn flag  */
    private const val WARN = true

    /** debug flag  */
    private const val DEBUG = true

    /** info flag */
    private const val INFO = true

    /** verbose flag  */
    private const val VERBOSE = true

    /**
     * @param msg
     */
    fun e(msg: String) {
        if (ERROR) {
            printLog("e", msg)
            callLog("e", msg)
        }
    }

    /**
     * @param msg
     * @param tr
     */
    fun e(msg: String, tr: Throwable?) {
        if (ERROR) {
            Log.e(getTag(4), msg, tr)
        }
    }

    /**
     * @param msg
     */
    fun w(msg: String) {
        if (WARN) {
            printLog("w", msg)
            callLog("w", msg)
        }
    }

    /**
     * @param msg
     */
    fun d(msg: String) {
        if (DEBUG) {
            printLog("d", msg)
            callLog("d", msg)
        }
    }

    /**
     * @param msg
     */
    fun i(msg: String) {
        if (INFO) {
            printLog("i", msg)
            callLog("i", msg)
        }
    }

    /**
     * @param msg
     */
    fun v(msg: String) {
        if (VERBOSE) {
            printLog("v", msg)
            callLog("v", msg)
        }
    }

    private fun callLog(flag: String, msg: String) {
        if (msg.length <= 3000) return
        val txt = msg.substring(3000)
        when (flag) {
            "e" -> e(txt)
            "v" -> v(txt)
            "i" -> i(txt)
            "d" -> d(txt)
            "w" -> w(txt)
        }
    }


    private fun printLog(flag: String, msg: String) {

        val txt = if (msg.length > 3000) msg.substring(0, 3000) else msg

        when (flag) {
            "e" -> Log.e(tag, txt)
            "v" -> Log.v(tag, txt)
            "i" -> Log.i(tag, txt)
            "d" -> Log.d(tag, txt)
            "w" -> Log.w(tag, txt)
        }
    }


    private fun getTag(number: Int): String {
        val clName =
            Thread.currentThread().stackTrace[number].className
        val array: List<String> = clName.split(".")
        val className = array[array.size - 1]
        val methodeName =
            Thread.currentThread().stackTrace[number].methodName
        //val line = Thread.currentThread().stackTrace[number].lineNumber
        return "$className - $methodeName"// [$line]"
    }

    private val tag: String
        get() {
            return getTag(6)
        }
}