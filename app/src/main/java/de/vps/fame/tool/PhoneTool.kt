package de.vps.fame.tool

import java.util.*

object PhoneTool {
    /**
     * get correctly phonenumber (parser)
     * @param phoneNumber phone number
     * @return number string
     */
    fun getPhoneNumber(phoneNumber: String): String {
        var number = phoneNumber
        if (number.contains("oder")) {
            number = number.substring(0, number.indexOf("oder"))
        }
        if (number.contains("o.")) {
            number = number.substring(0, number.indexOf("o."))
        }
        return number.replace(" ", "").replace("/", "").trim { it <= ' ' }
    }

    /**
     * check phonenumber
     * @param number phone number
     * @return phone number string
     */
    fun validPhoneNumber(number: String): Boolean {
        return !(number.isEmpty() ||
                number.toLowerCase(Locale.getDefault()).contains("k.") ||
                number.toLowerCase(Locale.getDefault()).contains("unten"))
    }
}