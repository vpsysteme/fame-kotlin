package de.vps.fame.network

import android.content.Context
import com.android.volley.Request
import com.android.volley.RequestQueue
import com.android.volley.Response
import com.android.volley.toolbox.StringRequest
import com.android.volley.toolbox.Volley

/**
 * Network connection with Volley
 */
class Network(context: Context) {

    private var url:String = ""
    private var listener = Response.Listener<String> {  }
    private var errorListener = Response.ErrorListener {  }

    private val queue : RequestQueue = Volley.newRequestQueue(context)

    fun setUrl(url: String): Network {
        this.url = url
        return this
    }

    fun setReponseListener (listener: Response.Listener<String>): Network {
        this.listener = listener
        return this
    }

    fun setErrorListener (errorListener: Response.ErrorListener): Network {
        this.errorListener = errorListener
        return this
    }

    fun sendRequest(tag: String): Network{
        val resquest = StringRequest(Request.Method.GET,url,listener,errorListener)
        resquest.tag =tag
        queue.add(resquest)
        return this
    }

    fun destroyConnection(tag: String) {
        queue.cancelAll(tag)
    }
}
